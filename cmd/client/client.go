package client

import (
	"context"

	"git.begroup.team/platform-core/kitchen/l"
	"git.begroup.team/platform-transport/gogi20/pb"
	"google.golang.org/grpc"
	"google.golang.org/grpc/balancer/roundrobin"
)

type Gogi20Client struct {
	pb.Gogi20Client
}

func NewGogi20Client(address string) *Gogi20Client {
	conn, err := grpc.DialContext(context.Background(), address,
		grpc.WithInsecure(),
		grpc.WithBalancerName(roundrobin.Name),
	)

	if err != nil {
		ll.Fatal("Failed to dial Gogi20 service", l.Error(err))
	}

	c := pb.NewGogi20Client(conn)

	return &Gogi20Client{c}
}
