// IM AUTO GENERATED, BUT CAN BE OVERRIDDEN

package main

import (
	"git.begroup.team/platform-transport/gogi20/internal/services"
	"git.begroup.team/platform-transport/gogi20/internal/stores"
	"git.begroup.team/platform-transport/gogi20/pb"
	"honnef.co/go/tools/config"
)

func registerService(cfg *config.Config) pb.Gogi20Server {

	mainStore := stores.NewMainStore()

	return services.New(cfg, mainStore)
}
